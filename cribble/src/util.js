import Papa from "papaparse"
import * as d3 from "d3-array"
import * as _ from "lodash";

/**
 * Folder containing dataset directories (in public folder)
 */
const DATA_PATH = "data"

/**
 * @param {*} arr
 * @param {number} q
 */
const quantile = (arr, q) => {
    const asc = arr => arr.sort((a, b) => a - b);
    const sorted = asc(arr);
    const pos = (sorted.length - 1) * q;
    const base = Math.floor(pos);
    const rest = pos - base;
    if (sorted[base + 1] !== undefined) {
        return sorted[base] + rest * (sorted[base + 1] - sorted[base]);
    } else {
        return sorted[base];
    }
};

/**
 * Fetch data supporting a view: config and CSV file for a dataset
 * @param {String} pathToConfig: path to config file, only depends on dataset
 * @param {String} pathToCSV: path to CSV file, depends on particular view
 * @param {Number} n: optional parameter to only read the n first rows of the CSV
 */
function fetchDataset(pathToConfig, pathToCSV, n = -1) {
    const configPromise = fetch(pathToConfig).then(response => response.json())
    const dataPromise = parseCSV(pathToCSV, n)
    return new Promise((resolve, reject) => {
        Promise
            .all([configPromise, dataPromise])
            .then((values) => {
                const config = values[0]
                const data = values[1]
                resolve([config, data])
            })
            .catch(reject)
    })
}

/**
 * Parse CSV file with Number, String or JSON in cells
 * @param {string} filepath:
 * @param {number} n: optional parameter to only read the n first rows
 */
function parseCSV(filepath, n = -1) {
    function parseJSON(data) {
        data.forEach(function (row) {
            Object.keys(row).forEach((col) => {
                // Detect and parse cells that contain JSON
                if (typeof row[col] === 'string' && row[col][0] === "{") {
                    row[col] = JSON.parse(row[col].replaceAll("'", '"'))
                }
            })
        })
        return data
    }

    return new Promise((resolve, reject) => {
        Papa.parse(filepath, {
            header: true,
            download: true,
            preview: n,
            dynamicTyping: true,
            skipEmptyLines: true,
            complete(results) {
                resolve(parseJSON(results.data))
            },
            error: reject
        })
    })
}

/**
 * Bin value using criteria and assign label from three categories:
 * "fail criteria", "close to criteria", "validate criteria"
 * @param {Object} config: config object
 * @param {String} criteria: KPI
 * @param {String} country: country
 * @param {Number} value: value to compare to threshold
 */
function compareThreshold(config, criteria, country, value) {
    const NEAR_ZERO_BOUND = 0.1
    console.assert(config)
    console.assert('threshold' in config && 'columnBest' in config && 'mapLabels' in config)
    const labels = config.mapLabels
    console.assert(labels.length === 3)
    /// If config does not specify a threshold or a "best" direction, categorize as "validate criteria"
    if (!(criteria in config.threshold) || !(country in config.threshold[criteria]))
        return {threshold: null, difference: -1, adequacy: labels[2]}
    const threshold = config.threshold[criteria][country]
    console.assert(threshold)
    const direction = config.columnBest[criteria]
    const difference = direction === "minimum" ? value - threshold : threshold - value
    const adequacy = Math.abs(difference) < NEAR_ZERO_BOUND ? labels[1] : difference < 0 ? labels[0] : labels[2]
    return {threshold, difference, adequacy}
}

/**
 * @param {Array} data
 * @param {String} criteria
 * @param {Boolean} reverse
 */
function sortCountryData(data, criteria, reverse) {
    return data.sort((r1, r2) => {
        if (typeof criteria === 'string' || criteria instanceof String) {
            console.assert(r1[criteria] && r2[criteria])
            const r1_ = r1[criteria]
            const r2_ = r2[criteria]
            if (typeof r1_ === 'string')
                return reverse ? r1_.localeCompare(r2_) : r2_.localeCompare(r1_)
            console.assert("avg" in r1_ && "avg" in r2_)
            return reverse ? r1_.avg - r2_.avg : r2_.avg - r1_.avg
        }
        console.assert("metaCol" in criteria && "col" in criteria)
        const r1_ = r1[criteria.metaCol][criteria.col].avg
        const r2_ = r2[criteria.metaCol][criteria.col].avg
        return reverse ? r1_ - r2_ : r2_ - r1_
    })
}

/**
 * @param {Array} acc - Array of bins, filled if empty, updated with counts from data otherwise
 * @param {Array} data - Array of values to bin
 * @param {Array} range - Two-value array for the min and max of the binned domain [min, max]
 * @param {Number} nbBins - Number of bins to use
 */
function binAccumulate(acc, data, range, nbBins) {
    const rangeLength = Math.abs(range[1] - range[0])
    if (rangeLength > 0) {
        const histGenerator = d3.bin().domain(range).thresholds(nbBins)
        const bins = histGenerator(data)
        if (acc.length === 0) {
            acc.push(...bins.map((b, i) => {
                return {
                    bin_start: range[0] + rangeLength * i / bins.length,
                    bin_end: range[0] + rangeLength * (i + 1) / bins.length,
                    count: b.length
                }
            }))
        }
        bins.forEach((b, i) => {
            acc[i].count += b.length
        })
    }
}

class DatasetProvider {
    // TODO: Reimplement with non-spaghetti code (alasql?)
    /**
     * @param {*} data
     * @param {string[]} columns
     * @param {string|number} groupBy
     */
    static computeColumnRanges(data, columns, groupBy = null) {
        let ranges = {}
        data.forEach((row) => {
            columns.forEach((col) => {
                if (groupBy) {
                    if (!(row[groupBy] in ranges))
                        ranges[row[groupBy]] = {}
                    if (!(col in ranges[row[groupBy]]))
                        ranges[row[groupBy]][col] = {}
                    ranges[row[groupBy]][col].min =
                        Math.min(row[col].min, ranges[row[groupBy]][col].min) || row[col].min
                    ranges[row[groupBy]][col].max =
                        Math.max(row[col].max, ranges[row[groupBy]][col].max) || row[col].max
                } else {
                    if (!(col in ranges))
                        ranges[col] = {}
                    ranges[col].min = Math.min(row[col].min, ranges[col].min) || row[col].min
                    ranges[col].max = Math.max(row[col].max, ranges[col].max) || row[col].max
                }
            })
        })
        return ranges
    }

    /** Query and format data for all countries. Each cell contains aggregate data across scenarios
     (avg, med, min, max). Only a small set of columns are returned.
     * @param {string} dataset
     */
    static getDatasetData(dataset) {
        const path = `${DATA_PATH}/${dataset}`
        return new Promise((resolve, reject) => {
            fetchDataset(`${path}/config.json`, `${path}/overview.csv`)
                .then(([config, data]) => {
                    console.assert(config.selectedColumns)
                    const ranges = DatasetProvider.computeColumnRanges(data, config.selectedColumns, "Plan")
                    // Format data: rows have keys "Country" and all plans
                    data = _.mapValues(_.groupBy(data, "Country"), r => _.groupBy(r, "Plan"))
                    data = _.toPairs(data).map(e => {
                        const country = e[0]
                            const planDict = _.mapValues(e[1], v => v[0])
                            return {Country: country, ...planDict}
                        }
                    )
                    resolve([config, data, ranges])
                })
                .catch(reject)
        })
    }


    /** Query and format data for one country. Each cell contains aggregate data across scenarios
     * (avg, med, min, max). All columns are returned.
     * @param {string} dataset
     * @param {string} country
     */
    static getCountryData(dataset, country) {
        const path = `${DATA_PATH}/${dataset}`
        return new Promise((resolve, reject) => {
            fetchDataset(`${path}/config.json`, `${path}/${country}.csv`)
                .then(([config, data]) => {
                    const columns = Object.keys(data[0]).filter(col => col !== "Plan")
                    const ranges = DatasetProvider.computeColumnRanges(data, columns)
                    // Format data: rows have keys "Plan", all columns and colMin and colMax
                    data.forEach((row) => {
                        columns.forEach((col) => {
                            row[col] = {...row[col], colMin: ranges[col].min, colMax: ranges[col].max}
                        })
                    })
                    resolve([config, data, ranges])
                })
                .catch(reject)
        })
    }

    /** Query and format data for one country and one plan (metaColumn). Each cell contains a value.
     All scenarios and columns are returned.
     * @param {string} dataset
     * @param {string} country
     * @param {string} metaColumn
     */
    static getScenarioData(dataset, country, metaColumn) {
        const path = `${DATA_PATH}/${dataset}`
        return new Promise((resolve, reject) => {
            fetchDataset(`${path}/config.json`, `${path}/${metaColumn}${country}.csv`)
                .then(([config, data]) => resolve([config, data])).catch(reject)
        })
    }

    static groupedScenarioData(config, data, country, categoryColumn, idColumn) {
        const FILTERED_LABELS = [config.mapLabels[2]] // Label for adequate values
        const columns = Object.keys(data[0]).filter(c => c !== "Scenario")
        // Might return multiple categories
        let categorizeByFailure = (row) => {
            return columns
                .map(c => [c, compareThreshold(config, c, country, row[c]).adequacy])
                .filter(([, adequacy]) => FILTERED_LABELS.indexOf(adequacy) < 0)
                .map(([col, adequacy]) => `${col} / ${adequacy}`)
        }
        let categorizeByYear = (row) => {
            return [row[idColumn].split('_')[0]]
        }
        return {
            'Fail': DatasetProvider.aggregateData(data, categorizeByFailure, categoryColumn, idColumn),
            'Year': DatasetProvider.aggregateData(data, categorizeByYear, categoryColumn, idColumn)
        }
    }

    static aggregateData(data, categoryFunc, categoryColumn, idColumn) {
        const columns = Object.keys(data[0]).filter(c => c !== idColumn)
        let groupByCategory = _.groupBy(
            data.map(row => {
                return {[categoryColumn]: categoryFunc(row), ...row}
            }),
            categoryColumn
        )
        let summarizeAllColumns = (rows) => {
            return _.fromPairs(
                columns.map(col => {
                    const values = rows.map(r => r[col])
                    return [col, {
                        avg: _.mean(values),
                        min: _.min(values),
                        max: _.max(values),
                        med: quantile(values, .50),
                        perc95: quantile(values, .95),
                        count: values.length
                    }]
                }))
        }
        // Aggregate
        return Object.entries(groupByCategory).map(([key, values]) => {
            return {[categoryColumn]: key, ...summarizeAllColumns(values)}
        })
    }

    /**
     * @param {string} dataset
     * @param {array} countries
     * @param {array} metaColumns
     * @param {array} columns
     * @param {Object} columnRanges
     * @param {number} nbBins
     * @param {number} sampleSize
     * @param {boolean} groupByMetaColumn
     */
    static async computeDistributions(dataset, countries, metaColumns, columns, columnRanges,
                                      nbBins, groupByMetaColumn, sampleSize = 100) {
        let res
        if (groupByMetaColumn) {
            res = Object.fromEntries(metaColumns.map(m => [m, Object.fromEntries(columns.map(c => [c, []]))]))
        } else {
            res = Object.fromEntries(columns.map(c => [c, []]))
        }
        for (const metaCol of metaColumns) {
            if (!(metaCol in res) && groupByMetaColumn) {
                res[metaCol] = {}
                for (const col of columns)
                    res[metaCol][col] = []
            } else {
                for (const col of columns)
                    res[col] = []
            }
            for (const country of countries) {
                const pathToFile = `${DATA_PATH}/${dataset}/${metaCol}${country}.csv`
                let data = await parseCSV(pathToFile, sampleSize).catch(console.error)
                for (const col of columns) {
                    if (groupByMetaColumn) {
                        const ranges = [columnRanges[metaCol][col].min, columnRanges[metaCol][col].max]
                        binAccumulate(res[metaCol][col], data.map(row => row[col]), ranges, nbBins)
                    } else {
                        const ranges = [columnRanges[col].min, columnRanges[col].max]
                        binAccumulate(res[col], data.map(row => row[col]), ranges, nbBins)
                    }
                }
            }
        }
        return res
    }
}

export {
    parseCSV,
    compareThreshold,
    binAccumulate,
    sortCountryData,
    DatasetProvider,
    DATA_PATH
}
