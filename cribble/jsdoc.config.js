module.exports = {
    plugins: [
        'node_modules/jsdoc-vuejs',
        'plugins/markdown'
    ],
    source: {
        include: [
            'src/',
            'src/components',
        ],
        includePattern: '\\.(vue|js)$',
        excludePattern: '(node_modules/|docs)'
    },
    templates: {
        cleverLinks: false,
        monospaceLinks: true,
        useLongnameInNav: false,
        showInheritedInNav: true
    },
    opts: {
        encoding: 'utf8',
        template: 'node_modules/minami',
        destination: 'docs',
        private: true,
        recurse: true
    },
};
